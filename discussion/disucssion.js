// MongoDB Operations
// For creating or inserting data into database

db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "534776",
        email: "ely@eraserheads.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
})


// for insert many

db.users.insertMany([
    {
        firstName: "Chito",
        lastName: "Miranda",
        age: 43,
        contact: {
            phone: "5347786",
            email: "chito@parokya.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Francis",
        lastName: "Magalona",
        age: 61,
        contact: {
            phone: "5347786",
            email: "francisM@email.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
])
// ==========================================

// for querying all the data in database
// For finding documents

//  FIND ALL
db.users.find();

// FIND [One]
db.users.find({firstName: "Francis", age:61 });

// DELETE Delete One

db.users.deleteOne({
    firstName: "Ely"
});

// Update One
db.users.updateOne(
    {
        firstName: "Gloc-9"
    },
    {
        $set: {
            department: "DOH"
        }
    }
);

//  Delete Many

db.users.deleteMany({
    department: "DOH"
});


// Update Many
db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);

//============================================