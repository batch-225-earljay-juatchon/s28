db.users.insert({
    name: "single",
    accomodates: "2.0",
    price: "1000.0",
    description: "A simple room with all the basic necessities",
    rooms_available: "10.0",
    isAvailable: "false"
})   

db.users.insertMany([
    {
        name: "double",
        accomadates: "3.0",
        price: "2000.0",
        description: "A room fit for a small family going on a vacation",
        rooms_available: "5.0",
        isAvailable: "false"
    },
    {
       name: "queen",
        accomadates: "4",
        price: "4000",
        description: "A room with a queen sized bed perfect for a simple gateway",
        rooms_available: "15",
        isAvailable: "false"
        }
        
])



db.users.find({name: "double"});



db.users.updateOne(
{
    name: "queen"
},
{
    $set: {
        rooms_available: 0
    }
}
)


db.users.deleteMany({
    rooms_available: 0,
});
